const navBar = document.getElementById("nav-bar")
const NavBoard = document.getElementById("board-option")
const NavHome = document.getElementById("home-option")
const boardsWrapper = document.getElementById("boards-wrapper")
const boardsContainer = document.getElementById("boards-container")
const homeDisplay = document.getElementsByClassName("home-display")
const header = document.getElementsByTagName("header")
const createBoard = document.getElementsByClassName("create-board")
const body = document.getElementsByTagName("body")
const listsWrapper = document.getElementById("lists-wrapper")
const addList = document.getElementById("add-list")
const sectionContainer = document.getElementsByClassName("section-container")
const feeds= document.getElementById("feeds")
const background = ["images/david-billings-Zmfz5jqgSiI-unsplash.jpg",
    "images/eberhard-grossgasteiger-KrNuV_xJJWI-unsplash.jpg",
    "images/kasra-assadian-QTiQ4ujnKeg-unsplash.jpg"]

NavBoard.addEventListener("click", displayBoards)
NavHome.addEventListener("click", displayHome)

function displayHome() {
    boardsContainer.style.display = "none";
    homeDisplay[0].style.display = "flex";
    // header[0].style.backgroundColor="#026aa7";
    NavBoard.style.border = "none";
    NavHome.style.border = "3px solid #63b6e6";
}

function displayBoards() {
    boardsContainer.style.display = "flex";
    homeDisplay[0].style.display = "none";
    // header[0].style.backgroundColor="grey";
    NavBoard.style.border = "3px solid #63b6e6";
    NavHome.style.border = "none"
    fetch("https://api.trello.com/1/members/me/boards?fields=name,url&key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7", {
        method: "GET", headers: {
            'Accept': 'application/json'
        }
    })
        .then((apiData) => {
            return apiData.json()
        })
        .then((data) => {
            return createBoards(data)                  
        })              
        .catch((error) => {
            console.error(error)
        })        
}


function clickedBoard(event){
    header[0].style.background="white";   
    header[0].style.opacity="0.5";   
    body[0].style.background =`url("images/eberhard-grossgasteiger-KrNuV_xJJWI-unsplash.jpg")`
    body[0].style.backgroundRepeat = "no-repeat"
    body[0].style.backgroundSize ="1900px"
    body[0].style.backgroundPosition ="center"
    let boardId= event.target.classList[0]
    
    navBar.style.display="none"
    boardsContainer.style.display="none"
    feeds.style.display="none";
    //sectionContainer.style.display="flex"
    sectionContainer[0].style.justifyContent="flex-start"
    sectionContainer[0].style.margin="10px 10px"
    sectionContainer[0].style.width="100%"
    sectionContainer[0].style.alignItems="flex-start"
    listsWrapper.style.display="flex";

    fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7`, 
    {
        method : "GET"
    }) 
    .then((urlData) => {
        return urlData.json()
    })
    .then((data) => {
        console.log(data)
       return createLists(data)
    })
    .catch((error) => {
        console.error(error)
    })
    
}

function createLists(data){
    if(listsWrapper.childElementCount<=1){
        for(const list of data){
            let newList = document.createElement("div")
            let listName = document.createElement("h4")            
            newList.id = "newly-created-list"
            newList.classList.add(list["id"])
            console.log(data)
            listName.innerHTML=list["name"]
            newList.appendChild(listName)
            newTextBox = document.createElement("input")
            newTextBox.setAttribute("placeHolder","Add a card")
            newTextBox.style.borderRadius="5px"
            newTextBox.style.width="280px"
            newTextBox.style.height="30px"
            newList.appendChild(newTextBox)
            listsWrapper.insertBefore(newList, addList)
            fetchCards(list["id"])            
        }
    }
   
}

function fetchCards(cardId){
    fetch(`https://api.trello.com/1/lists/${cardId}/cards?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7`,
    {
        method:"GET"
    })
    .then((urlData) => {
        return urlData.json()
    })
    .then((data) =>{       
        createCards(data)
        return data
    })
    .catch((error) => {
        console.error(error)
    })
}

function createCards(data){    
    for(const card of data){
        const parentList=document.getElementsByClassName(card["idList"])
        console.log(parentList[0])
        const newCard = document.createElement("div")
        const cardName = document.createElement("p")
        newCard.id="newly-created-card"
        newCard.classList.add(card["id"])
        cardName.innerHTML=card["name"]
        newCard.addEventListener("click", displayCard)
        newCard.appendChild(cardName)        
        parentList[0].appendChild(newCard)
    }
    
}

function displayCard(){
    
}

function createBoards(data){
    if(boardsWrapper.childElementCount<=1)
    {
        for (const board of data) {
            let boardName = (board["name"])
            let newDiv = document.createElement("div")
            newDiv.id = "newly-created-board"
            newDiv.classList.add(board["id"])                
            let newText = document.createElement("h2")
            newText.innerHTML=boardName
            //console.log(newDiv)
            newDiv.appendChild(newText)   
            console.log(newDiv.classList[0])
            newDiv.addEventListener("click", clickedBoard)                 
            boardsWrapper.insertBefore(newDiv, createBoard[0])
        }
    }  
}