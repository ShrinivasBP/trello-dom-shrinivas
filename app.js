const express = require("express")
const path = require("path")

const config = require("./config.js")

const PORT = config.PORT

let app = express()

app.use("/images", express.static(path.join(__dirname, "/images")))

app.use("/images/button", express.static(path.join(__dirname, "/images/button")))

app.use("/css", express.static(path.join(__dirname, "/css")))

app.use("/JavaScript", express.static(path.join(__dirname, "/JavaScript")))

app.get("/", (request, response) => {
    response.sendFile(path.join(__dirname, "./index.html"))
})

app.get("/boards", (request, response) => {
    response.sendFile(path.join(__dirname, "./boards.html"))
})

app.get("/boards/:Id", (request, response) => {
    response.sendFile(path.join(__dirname, "./display.html"))
})

app.listen(PORT, (error) => {
    if (error) {
        console.error("error")
    }
    else {
        console.log(`server is listening at port ${PORT}`)
    }
}) 