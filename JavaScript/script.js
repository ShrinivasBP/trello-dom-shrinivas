const navBar = document.getElementById("nav-bar")
const navBoard = document.getElementById("board-option")
const NavHome = document.getElementById("home-option")
const boardsWrapper = document.getElementById("boards-wrapper")
const boardsContainer = document.getElementById("boards-container")
const homeDisplay = document.getElementsByClassName("home-display")
const header = document.getElementsByTagName("header")
const createBoard = document.getElementsByClassName("create-board")
const body = document.getElementsByTagName("body")
const listsWrapper = document.getElementById("lists-wrapper")
const addList = document.getElementById("add-list")
const sectionContainer = document.getElementsByClassName("section-container")
const feeds= document.getElementById("feeds")
const cardPage = document.getElementById("card-page")
const headerHome = document.getElementById("home-button")
const headerBoard = document.getElementById("board-button")

headerHome.addEventListener("click", () => {
    document.location="./"
})
headerBoard.addEventListener("click", () => {
    document.location="./boards"
})
navBoard.addEventListener("click", () => {
    document.location="./boards"
})
