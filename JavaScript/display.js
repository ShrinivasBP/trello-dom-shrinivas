const navBar = document.getElementById("nav-bar")
const NavBoard = document.getElementById("board-option")
const NavHome = document.getElementById("home-option")
const boardsWrapper = document.getElementById("boards-wrapper")
const boardsContainer = document.getElementById("boards-container")
const homeDisplay = document.getElementsByClassName("home-display")
const header = document.getElementsByTagName("header")
const createBoard = document.getElementsByClassName("create-board")
const body = document.getElementsByTagName("body")
const listsWrapper = document.getElementById("lists-wrapper")
const addList = document.getElementById("add-list")
const sectionContainer = document.getElementsByClassName("section-container")
const feeds = document.getElementById("feeds")
const cardPage = document.getElementById("card-page")
// const checkListContainer = document.getElementById("checkList-container")
const headerHome = document.getElementById("home-button")
const headerBoard = document.getElementById("board-button")

headerHome.addEventListener("click", () => {
    document.location = "../"
})
headerBoard.addEventListener("click", () => {
    document.location = "../boards"
})

const urlPath = document.location.pathname.split("/")
const boardId = urlPath[2]
const background = localStorage.getItem("background")
console.log(background)
console.log(body[0])
body[0].style.background = background
body[0].style.backgroundSize = "100vw 100vh"
body[0].style.backgroundRepeat = "no-repeat"
body[0].style.backgroundPosition = "cover"


fetch(`https://api.trello.com/1/boards/${boardId}?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7`,
    {
        method: 'GET'
    })
    .then((urlData) => {
        return urlData.json()
    })
    .then((board) => {
        const headerButtonsContainer = document.getElementById("buttons-container")
        const boardTitle = document.createElement("button")
        boardTitle.innerHTML = board["name"]
        headerButtonsContainer.appendChild(boardTitle)
        console.log(header)
        return fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7`,
            {
                method: "GET"
            })
    })
    .then((urlData) => {
        return urlData.json()
    })
    .then((data) => {
        console.log(data)
        return createLists(data)
    })
    .catch((error) => {
        console.error(error)
    })

function createLists(data) {
    if (listsWrapper.childElementCount <= 1) {
        for (const list of data) {
            const newList = document.createElement("div")
            const listName = document.createElement("h4")
            newList.id = "newly-created-list"
            newList.classList.add(list["id"])
            listName.innerHTML = list["name"]
            newList.appendChild(listName)
            const addCardWrapper = document.createElement("div")
            addCardWrapper.id = "add-card-wrapper"
            newTextBox = document.createElement("input")
            newTextBox.setAttribute("placeHolder", "Add a card ")
            newTextBox.setAttribute("required", "")
            newTextBox.id = "text-box"
            newTextBox.classList.add(list["id"])
            const addButton = document.createElement("button")
            addButton.innerHTML = "+"
            addButton.classList.add(list["id"])
            addButton.id = "add-card-button"
            addButton.addEventListener("click", createNewCard)
            addCardWrapper.appendChild(newTextBox)
            addCardWrapper.appendChild(addButton)
            newList.appendChild(addCardWrapper)
            listsWrapper.insertBefore(newList, addList)
            fetchCards(list["id"])
        }
    }
}

function fetchCards(cardId) {
    fetch(`https://api.trello.com/1/lists/${cardId}/cards?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7`,
        {
            method: "GET"
        })
        .then((urlData) => {
            return urlData.json()
        })
        .then((data) => {
            createCards(data)
            return data
        })
        .catch((error) => {
            console.error(error)
        })
}

function createCards(data) {
    let index = 0
    for (const card of data) {
        const parentList = document.getElementsByClassName(card["idList"])
        const beforeTextBox = parentList[0].children[++index]
        const newCard = document.createElement("div")
        const cardName = document.createElement("p")
        newCard.id = "newly-created-card"
        newCard.classList.add(card["id"])
        cardName.innerHTML = card["name"]
        cardName.classList.add(card["id"])
        newCard.addEventListener("click", displayCard)
        newCard.appendChild(cardName)
        const closeButton = document.createElement("button")
        closeButton.innerHTML = "x"
        closeButton.id = "delete-card-button"
        closeButton.classList.add(card["id"])
        newCard.appendChild(closeButton)
        closeButton.addEventListener("click", deleteCard)
        parentList[0].insertBefore(newCard, beforeTextBox)
    }
}

const deleteCard = (event) => {
    event.stopPropagation()
    const verify = window.confirm("DO u want to delete the card")
    if (verify) {
        fetch(`https://api.trello.com/1/cards/${event.target.parentNode.classList[0]}?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7`, {
            method: 'DELETE'
        })
            .then(() => {
                event.target.parentNode.remove()
                console.log("Successfully deleted the card " + event.target.parentNode.classList[0])
            })
            .catch((error) => {
                console.error(error)
            })
    }
}

function displayCard(event) {
    const cardDisplay = document.createElement("div")
    cardDisplay.id = "card-page"

    fetch(`https://api.trello.com/1/cards/${event.target.classList[0]}?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7`)
        .then((urlData) => {
            return urlData.json()
        })
        .then((data) => {
            const cardDisplayContainer = document.createElement("div")
            cardDisplayContainer.id = "card-display-container"
            body[0].appendChild(cardDisplayContainer)
            const cardName = document.createElement("h2")
            cardName.innerHTML = data["name"]
            cardName.id = "card-title"
            cardName.classList.add(data["id"])
            cardDisplay.append(cardName)
            const descWrapper = document.createElement("div")
            descWrapper.classList.add(data["id"])
            const description = document.createElement("h2")
            description.innerHTML = "Description"
            description.id = "card-desc"
            description.classList.add(data["id"])
            descWrapper.appendChild(description)
            const descBox = document.createElement("pre")
            // descBox.setAttribute("contenteditable", "true")
            // descBox.setAttribute("cols", "60")
            // descBox.setAttribute("rows", "10")
            descBox.innerHTML = data["desc"]
            descBox.id = "card-desc-text"
            descBox.classList.add(data["id"])
            descBox.addEventListener("click", (event) => {
                const checkListContainer = document.getElementById("checkList-container")
                const textBox = document.createElement("textarea")
                textBox.id = "desc-textarea"
                textBox.innerHTML = event.target.innerHTML
                textBox.setAttribute("cols", "50")
                textBox.setAttribute("rows", "20")
                event.target.parentElement.remove()
                cardDisplay.insertBefore(textBox, checkListContainer)
            })
            descWrapper.appendChild(descBox)
            cardDisplay.appendChild(descWrapper)
            const checkList = fetchCheckLists(data["id"], cardDisplay)
            const cross = document.createElement("button")
            cross.id = "page-close-button"
            cross.innerHTML = "X"
            cardDisplay.appendChild(cross)
            cardDisplayContainer.append(cardDisplay)
            cross.addEventListener("click", () => {
                cardDisplay.parentElement.remove()
            })
            const addListButton = document.createElement("button")
            addListButton.id = "add-list-button"
            addListButton.classList.add(data["id"])
            addListButton.innerHTML = "Add new List"
            cardDisplay.appendChild(addListButton)
            addListButton.addEventListener("click", addNewList)
        })
        .catch((error) => {
            console.error(error)
        })
}

const createNewCard = (event) => {

    let newCardName = event.target.previousElementSibling.value
    if (newCardName === "") {
        return
    }
    let listId = event.target.classList[0]
    fetch(`https://api.trello.com/1/cards?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7&name=${newCardName}&idList=${listId}`, {
        method: 'POST'
    })
        .then((urlData) => {
            console.log("successfully created a card")
            return urlData.json()
        })
        .then((card) => {
            console.log(card["id"])
            const newCard = document.createElement("div")
            const cardName = document.createElement("p")
            newCard.id = "newly-created-card"
            newCard.classList.add(card["id"])
            cardName.innerHTML = card["name"]
            cardName.classList.add(card["id"])
            newCard.addEventListener("click", displayCard)
            newCard.appendChild(cardName)
            const closeButton = document.createElement("button")
            closeButton.innerHTML = "x"
            closeButton.id = "delete-card-button"
            closeButton.classList.add(card["id"])
            newCard.appendChild(closeButton)
            closeButton.addEventListener("click", deleteCard)
            const list = event.target.parentNode.parentNode
            const addCardWrapper = event.target.parentNode
            list.insertBefore(newCard, addCardWrapper)
            event.target.previousElementSibling.value = ""
            console.log(newCardName)
        })
        .catch((error) => {
            console.error(error)
        })
}

const fetchCheckLists = (cardId, cardDisplay) => {
    fetch(`https://api.trello.com/1/cards/${cardId}/checklists?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7`, {
        method: "GET"
    })
        .then((urlData) => {
            return urlData.json()
        })
        .then((data) => {
            const sortedDataOfCheckLists = data.sort((obj1, obj2) => {
                if (obj1["pos"] > obj2["pos"]) {
                    return 1
                }
                else {
                    return -1
                }
            })
            const checkListsContainer = document.createElement("div")
            checkListsContainer.id = "checkList-container"
            checkListsContainer.classList.add(cardId)
            const checkListsContainerTitle = document.createElement("h2")
            checkListsContainerTitle.innerHTML = "CheckLists"
            checkListsContainer.appendChild(checkListsContainerTitle)
            // checkListsContainer.id = "checklist-wrapper"
            for (const checkList of sortedDataOfCheckLists) {
                const checkListWrapper = document.createElement("div")
                checkListWrapper.id = "checklist-wrapper"
                checkListWrapper.classList.add(checkList["id"])
                const checkListTitle = document.createElement("h3")
                checkListTitle.innerHTML = checkList["name"]
                checkListWrapper.appendChild(checkListTitle)
                const sortedDataOfCheckItems = checkList["checkItems"].sort((obj1, obj2) => {
                    if (obj1["pos"] > obj2["pos"]) {
                        return 1
                    }
                    else {
                        return -1
                    }
                })
                for (const checkItem of sortedDataOfCheckItems) {
                    const checkItemWrapper = document.createElement("div")
                    checkItemWrapper.id = "checkItem-wrapper"
                    checkItemWrapper.classList.add(checkItem["idChecklist"])
                    const checkBox = document.createElement("input")
                    checkBox.classList.add(checkItem["id"])
                    checkBox.type = "checkbox"
                    checkBox.id = "checkItems-checkBox"
                    if (checkItem["state"] === "complete")
                        checkBox.checked = "true"
                    const checkBoxTitle = document.createElement("p")
                    checkBoxTitle.innerHTML = checkItem["name"]
                    checkBox.addEventListener("click", toggleCheckBox)
                    const deleteCheckItemButton = document.createElement("button")
                    deleteCheckItemButton.innerHTML = "x"
                    deleteCheckItemButton.id = "delete-checkItem-button"
                    deleteCheckItemButton.classList.add(checkItem["id"])
                    checkItemWrapper.appendChild(checkBox)
                    checkItemWrapper.appendChild(checkBoxTitle)
                    checkItemWrapper.appendChild(deleteCheckItemButton)
                    checkListWrapper.appendChild(checkItemWrapper)
                    deleteCheckItemButton.addEventListener("click", deleteCheckItem)
                }
                checkListsContainer.appendChild(checkListWrapper)
                const addCheckItemsButton = document.createElement("button")
                addCheckItemsButton.innerHTML = "add Item"
                addCheckItemsButton.id = "add-item-button"
                addCheckItemsButton.classList.add(checkList["id"])
                addCheckItemsButton.addEventListener("click", addNewCheckItem)
                const deleteListButton = document.createElement("button")
                deleteListButton.id = "delete-list-button"
                deleteListButton.innerHTML = "delete"
                deleteListButton.classList.add(checkList["id"])
                deleteListButton.addEventListener("click", deleteList)
                checkListWrapper.insertBefore(deleteListButton, checkListTitle)
                checkListWrapper.appendChild(addCheckItemsButton)
            }
            cardDisplay.appendChild(checkListsContainer)
        })
        .catch((error) => {
            console.log(error)
        })
}

const toggleCheckBox = (event) => {
    const cardId = event.target.parentElement.parentElement.parentElement.classList[0]
    const checklistId = event.target.parentElement.parentElement.classList[0]
    const checkItemId = event.target.classList[0]
    console.log(cardId, checklistId, checkItemId)
    const currentState = (event.target.checked)
    if (currentState == true) {
        fetch(`https://api.trello.com/1/cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7&state=complete`, {
            method: "PUT"
        })
            .then(() => {
                console.log("status has been changed to checked")
            })
            .catch((error) => {
                console.error(error)
            })
    }
    else {
        fetch(`https://api.trello.com/1/cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7&state=incomplete`, {
            method: "PUT"
        })
            .then(() => {
                console.log("status has been changed to unchecked")
            })
            .catch((error) => {
                console.error(error)
            })
    }
}

const addNewList = (event) => {
    console.log(event)
    const newList = document.createElement("div")
    newList.id = "new-list"
    const listTitle = document.createElement("input")
    // listTitle.type = "text"
    listTitle.placeholder = "new checkList"
    newList.appendChild(listTitle)
    const createNewListButton = document.createElement("button")
    createNewListButton.id = "create-new-list-button"
    createNewListButton.innerHTML = "Add list"
    createNewListButton.classList.add(event.target.classList[0])
    createNewListButton.addEventListener("click", createNewList)
    newList.appendChild(createNewListButton)
    event.target.parentElement.append(newList)
    const createListCloseButton = document.createElement("button")
    createListCloseButton.id = "create-list-close-button"
    createListCloseButton.innerHTML = "x"
    createListCloseButton.addEventListener("click", closeCreateNewListBlock)
    newList.appendChild(createListCloseButton)
    newList.style.display = "flex"
}

const createNewList = (event) => {
    const cardId = event.target.classList[0]
    const listTitle = event.target.previousElementSibling.value
    if (listTitle === "") {
        return
    }
    const position = "bottom"
    fetch(`https://api.trello.com/1/checklists?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7&idCard=${cardId}&name=${listTitle}&pos=${position}`, {
        method: 'POST'
    })
        .then((urlData) => {
            return urlData.json()
        })
        .then((list) => {
            const checkListContainer = document.getElementById("checkList-container")
            const newCheckListWrapper = document.createElement("div")
            newCheckListWrapper.classList.add(list["id"])
            newCheckListWrapper.id = "checklist-wrapper"
            const newCheckListTitle = document.createElement("h3")
            newCheckListTitle.innerHTML = list["name"]
            newCheckListWrapper.appendChild(newCheckListTitle)
            const addCheckItemsButton = document.createElement("button")
            addCheckItemsButton.innerHTML = "add Item"
            addCheckItemsButton.id = "add-item-button"
            addCheckItemsButton.classList.add(list["id"])
            addCheckItemsButton.addEventListener("click", addNewCheckItem)
            const deleteListButton = document.createElement("button")
            deleteListButton.id = "delete-list-button"
            deleteListButton.innerHTML = "delete"
            deleteListButton.classList.add(list["id"])
            deleteListButton.addEventListener("click", deleteList)
            newCheckListWrapper.insertBefore(deleteListButton, newCheckListTitle)
            newCheckListWrapper.appendChild(addCheckItemsButton)
            checkListContainer.appendChild(newCheckListWrapper)
        })
        .catch((error) => {
            console.error(error)
        })
    console.log(event.target.classList[0])
    event.target.previousElementSibling.value = ""
    event.target.parentElement.style.display = "none"
}

const closeCreateNewListBlock = (event) => {
    event.stopPropagation()
    event.target.parentElement.style.display = "none"
}

const deleteList = (event) => {
    const deleteConfirmation = window.confirm("are you sure, you want to delete it?")
    if (deleteConfirmation) {
        const checkListId = (event.target.classList[0])

        fetch(`https://api.trello.com/1/checklists/${checkListId}?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7`, {
            method: 'DELETE'
        })
            .then(() => {
                event.target.parentElement.remove()
            })
            .catch((error) => {
                console.error(error)
            })
    }
}

const addNewCheckItem = (event) => {
    const checkListId = event.target.classList[0]
    const newCheckItem = document.createElement("div")
    newCheckItem.id = "new-checkitem"
    const textBox = document.createElement("textarea")
    textBox.cols = "60"
    textBox.rows = "4"
    newCheckItem.appendChild(textBox)
    const addButton = document.createElement("button")
    addButton.id = "add-button"
    addButton.innerHTML = "Add"
    addButton.classList.add(event.target.classList[0])
    const buttonWrapper = document.createElement("div")
    buttonWrapper.id = "button-wrapper"
    const closeButton = document.createElement("button")
    closeButton.id = "close-button"
    closeButton.innerHTML = "X"
    closeButton.classList.add(event.target.classList[0])
    buttonWrapper.appendChild(addButton)
    addButton.addEventListener("click", (event) => {
        const checkItemTitle = textBox.value
        const checkListId = event.target.classList[0]
        fetch(`https://api.trello.com/1/checklists/${checkListId}/checkItems?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7&name=${checkItemTitle}`, {
            method: 'POST'
        })
            .then((urlData) => {
                return urlData.json()
            })
            .then((checkItem) => {
                textBox.value = ""

                const checkList = event.target.parentElement.parentElement.parentElement

                newCheckItem.remove()
                const checkItemWrapper = document.createElement("div")
                checkItemWrapper.id = "checkItem-wrapper"
                checkItemWrapper.classList.add(checkItem["id"])
                const checkItemCheckBox = document.createElement("input")
                checkItemCheckBox.classList.add(checkItem["id"])
                checkItemCheckBox.id = "checkItems-checkBox"
                checkItemCheckBox.type = "checkBox"
                checkItemCheckBox.addEventListener("click", toggleCheckBox)
                const newCheckItemTitle = document.createElement("p")
                newCheckItemTitle.innerHTML = checkItem["name"]
                const deleteCheckItemButton = document.createElement("button")
                deleteCheckItemButton.innerHTML = "x"
                deleteCheckItemButton.id = "delete-checkItem-button"
                deleteCheckItemButton.classList.add(checkItem["id"])
                checkItemWrapper.appendChild(checkItemCheckBox)
                checkItemWrapper.appendChild(newCheckItemTitle)
                checkItemWrapper.appendChild(deleteCheckItemButton)
                deleteCheckItemButton.addEventListener("click", deleteCheckItem)
                checkList.lastChild.style.display = "flex"
                checkList.insertBefore(checkItemWrapper, checkList.lastChild)
                console.log(checkList.lastChild)
            })
            .catch((error) => {
                console.error(error)
            })
    })
    closeButton.addEventListener("click", (event) => {
        const checkList = event.target.parentElement.parentElement.parentElement
        checkList.lastChild.style.display = "flex"
        newCheckItem.remove()
    })
    buttonWrapper.appendChild(closeButton)
    newCheckItem.appendChild(buttonWrapper)
    event.target.parentElement.insertBefore(newCheckItem, event.target)
    event.target.style.display = "none"
}

const deleteCheckItem = (event) => {
    const deleteConfirmation = window.confirm("are you sure, you want to delete it?")
    if (deleteConfirmation) {
        event.target.parentElement.remove()
        const checkItemId = event.target.classList[0]
        const checklistId = event.target.parentElement.classList[0]
        fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7`, {
            method: 'DELETE'
        })
            .then(() => {
                console.log("deleted the checkItem")
            })
            .catch((error) => {
                console.error(error)
            })
    }
}