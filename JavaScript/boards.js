const navHome = document.getElementById("home-option")
const navBar = document.getElementById("nav-bar")
const NavBoard = document.getElementById("board-option")
const boardsWrapper = document.getElementById("boards-wrapper")
const boardsContainer = document.getElementById("boards-container")
const homeDisplay = document.getElementsByClassName("home-display")
const header = document.getElementsByTagName("header")
const createBoard = document.getElementsByClassName("create-board")
const body = document.getElementsByTagName("body")
const listsWrapper = document.getElementById("lists-wrapper")
const addList = document.getElementById("add-list")
const sectionContainer = document.getElementsByClassName("section-container")
const feeds= document.getElementById("feeds")
const cardPage = document.getElementById("card-page")
const headerHome = document.getElementById("home-button")
const headerBoard = document.getElementById("board-button")

headerHome.addEventListener("click", () => {
    document.location="./"
})
headerBoard.addEventListener("click", () => {
    document.location="./boards"
})

const background = ["/images/1.jpg",
"/images/2.jpg",
"/images/3.jpg",
"/images/4.jpg",
"/images/5.jpg",
"/images/6.jpg",
"/images/7.jpg",
"/images/8.jpg",
"/images/9.jpg",
"/images/10.jpg",
"/images/11.jpg",
"/images/12.jpg",
"/images/13.jpg",
"/images/14.jpg",
"/images/15.jpg",
"/images/16.jpg",
"/images/17.jpg",
"/images/18.jpg",
"/images/19.jpg",
"/images/20.jpg",
"/images/21.jpg"
]

navHome.addEventListener("click", () => {
    document.location="/"
})

fetch("https://api.trello.com/1/members/me/boards?fields=name,url&key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7", {
        method: "GET", headers: {
            'Accept': 'application/json'
        }
    })
.then((apiData) => {
    return apiData.json()
})
.then((data) => {
    return createBoards(data)                  
})              
.catch((error) => {
    console.error(error)
})       

function createBoards(data){
    if(boardsWrapper.childElementCount<=1){
        for (const board of data) {
            let boardName = (board["name"])
            let newDiv = document.createElement("div")
            newDiv.id = "newly-created-board"
            newDiv.classList.add(board["id"])   
            const randomNumber = Math.floor(Math.random()*20);
            newDiv.style.background = `rgba(0,0,0,0.8) url(${background[randomNumber]})`            
            newDiv.style.backgroundRepeat = "no-repeat"
            localStorage.setItem("background", `${background[randomNumber]}`)
            newDiv.style.backgroundPosition = "center";
            newDiv.style.backgroundSize = "cover"
            let newText = document.createElement("h2")
            newText.classList.add(board["id"])
            newText.innerHTML=boardName
            //console.log(newDiv)
            newDiv.appendChild(newText)   
            console.log(newDiv.classList[0])
            newDiv.addEventListener("click", clickedBoard)                 
            boardsWrapper.insertBefore(newDiv, createBoard[0])
        }
    }  
}

function clickedBoard(event){
    localStorage.setItem("background", event.target.style.backgroundImage)
    // console.log(error)
    document.location=`/boards/${event.target.classList[0]}`
    
    // header[0].style.background="white";   
    // header[0].style.opacity="0.5";   
    // body[0].style.background =`url("./images/1.jpg")`
    // body[0].style.backgroundRepeat = "no-repeat"
    // body[0].style.backgroundSize ="100vw 100vh"
    // body[0].style.backgroundPosition ="cover"
    // console.log(body[0])
    // let boardId= event.target.classList[0]    
    // navBar.style.display="none"
    // boardsContainer.style.display="none"      
    // //sectionContainer.sIddisplay="flex";
    

    // fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7`, 
    // {
    //     method : "GET"
    // }) 
    // .then((urlData) => {
    //     return urlData.json()
    // })
    // .then((data) => {
    //     console.log(data)
    //    //return createLists(data)
    // })
    // .catch((error) => {
    //     console.error(error)
    // })    
}
